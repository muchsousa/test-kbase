const dotenv = require('dotenv')
dotenv.config();

module.exports = {
    mongo: {
        db: process.env.MONGO_DB || '',
        host: process.env.MONGO_HOST || '',
        port: process.env.MONGO_PORT || '',
        user: process.env.MONGO_USER || '',
        pass: process.env.MONGO_PASS || '',
    },
    personConfig: {
        update: JSON.parse(process.env.PERSON_CONFIG_UPDATE) || false,
        create: JSON.parse(process.env.PERSON_CONFIG_CREATE) || false,
    },
    api: {
        url: process.env.API_URL || 'https://kbase-core-test.herokuapp.com',
    },
}