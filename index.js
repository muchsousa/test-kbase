const fetch = require('node-fetch'),
      Promise = require('promise'),
      config = require('./config'),
      mongo = require('./service/mongo')

// faz a requisição para recuperar o endereço e printa os dados
const personAddress = (person) => { 
  return fetch(`${config.api.url}/people/${person.id}/address`, { method: 'get' })
    .then(res => {
        if (!res.ok)
          // Tenta buscar o endereco novamente
          return personAddress(person, config.personConfig.create, config.personConfig.update)

        return res.json()
    })
    .then(json => {
      if (!json.address)
        return personAddress(person, config.personConfig.create, config.personConfig.update)

      // Aplica a lógica de update  
      if(config.personConfig.update) {
        const firstWordAddress = json.address.split(' ')[0];
        mongo.findOneAndUpdate(
          { 
            address: { $regex: `/^${firstWordAddress}/`, $options: 'i' } 
          },
          {
            $set: { name: person.name },
            $unset: { id: 1 }
          },
          { upsert: true }
        ).then()

      } 

      return {
        ...person,
        'address': json.address
      }
    })
    .catch(err => {
      console.log(err)

      return personAddress(person, config.personConfig.create, config.personConfig.update)
    })
};

// função que percorre 
const personBlocks = async (persons) => {

  // Define o tamanho do bloco
  const blockSize = 1000 
  // Calcula o numero de steps
  const totalSize = persons.length / blockSize 

  // Roda os Steps
  for (let i = 0; i < totalSize; i++) {

    let initial = (i * blockSize), final = ((i + 1) * blockSize)

    // Cuida para a posição final não ultrapassar o limite do array
    if (((i + 1) * blockSize) >= persons.length) 
      final = persons.length

    // Quebra um bloco de um determinado tamanho do array principal  
    let blockStep = persons.slice(initial, final)

    // monta o array de Promises
    blockStep = blockStep.map(personAddress)

    console.log(`Index initial = ${initial} - Index final = ${final}`)

    // espera executar todas Promises e printa
    await Promise.all(blockStep)
      .then(res => {
        console.log(res)
        
        // Cria o registro no mongo
        if(config.personConfig.create) 
          mongo.insertMany(res)
      })

  }
}

// pega a lista de pessoas
fetch(`${config.api.url}/people`, { method: 'get' })
  .then(res => {
    if (!res.ok)
      throw (new Error(res.statusText))

    return res.json()
  })
  .then(res => personBlocks(res))
  .catch(err => console.log(err));
