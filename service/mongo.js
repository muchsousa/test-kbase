const config = require('../config'),
      mongoose = require('mongoose');

const conf = config.mongo      
const mongoUrl = `mongodb://${conf.user}:${conf.pass}@${conf.host}:${conf.port}/${conf.db}`     
// const mongoUrl = `mongodb://${config.mongo.host}:${config.mongo.port}`

// Conecta no mongo
mongoose.connect(mongoUrl);

// Cria o Schema de person
const Schema = mongoose.Schema;
const personSchema = new Schema({
  id:  {
    type: String,
    required: false
  },
  name: String,
  address: String
}, { 
  collection: 'api', 
  versionKey: false 
});

// Cria e exporta a model de Person
module.exports = mongoose.model('person', personSchema);